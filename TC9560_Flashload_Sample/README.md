Release Date: Feb. 24, 2016
release Version V2.2
===============================================================================

Introduction:
=============
The folder contains a Keil project, which uses SPI to program Flash device on Neutrino evaluation/reference board.

Requirements:
============
1. You will need to have Keil nVision IDE Version 5 (Version 4 should be OK. TAEC tested with Version 5). For the earlier
   version of firmware (before 20160202), a valid Keil licence is required, as the fw.h size is bigger, and the image size
   is larger than 32k. For the newer firmware, the fw size is minimized, the final image would be smaller than 32k. Thus,
   an evaluation licence should be OK.
2. Keil ULink-me JTAG connector is used.

Procedures
==========
1. M3 firmware is in the form header file. TAEC will provide such firmware header file. Copy the firmware 
   header file to "Flashloader_xxxx\Src", and name it as "fw.h". In the release package, a couple of
   header files are included:
   * v1.0-20150920 : fw_SWSeq.h: POR software sequence is used. 
   * v1.1-20152012 : fw_HWSeq_IVI.h: POR hardware sequence is used. This is used by customer.
   * v1.2-20152012 : fw_HWSeq_Mdm.h: POR hardware sequence is used. This is used by customer.
   * v1.3-20160129 : fw_HWSeq_emac_tdm_can.h: POR hardware sequence is used. Supports MSI for EMAC, TDM and CAN module. 
					 Driver should take care of unmasking the interrupt at the end of ISR execution. 
   * v2.0-20160202 : fw_OSLess_HWSeq_emac_tdm_can.h: POR hardware sequence is used. Supports MSI for EMAC, TDM and CAN module. 
					 Driver should take care of unmasking the interrupt at the end of ISR execution. This is the default 
				     fw used in TAEC.
   * v2.2-20160224 : fw_OSLess_HWSeq_WoCAN.h: fw_OSLess_HWSeq_emac_tdm_can.h + Wake on CAN with initial testing.
	
   Note: Default "fw.h" is copy of "fw_OSLess_HWSeq_WoCAN.h"
   
2. Build the project using Keil IDE.
3. Download it to Neutrino SRAM and running it.
4. Open UART/Serial console with 115200 baud rate, 8data bits, 1stops bits no flow control.
5. On UART/Serial console, there is selection menu.
		Please select the test number:
		1. Prog FW to flash
		2. Compare FW data
		3. Prog pattern data
		9. Quit
6. Make selection: first "1", then "2".


Notes for Version 2.1 and later
===============================
1. When the fw.h file provides "fw_ver" and "fw_name" infomation, the flashloader program will 
   program such information to flash at flash offset:
   * 0x0220:  fw_ver information
   * 0x0240:  fw_name information

Notes for Version 2.0 and later
===============================
 1. fw_OSLess_HWSeq_emac_tdm_can.h is generated with 
    #define NTN_M3POR_4PCIE_ENABLE   		 	DEF_DISABLED
	
	fw_OSLess_SWSeq_emac_tdm_can.h is generated with 
	#define NTN_M3POR_4PCIE_ENABLE   			DEF_ENABLED
	
2. The firmware contains some debugging counters:
	#define  NTN_M3_DBG_CNT_START       0x0000C800  // Debugging count SRAM area start address
	/* NTN_M3_DBG_CNT_START + 4*0: 		DMA TX0
	 *   .........................
	 * NTN_M3_DBG_CNT_START + 4*4:  	DMA TX4
	 * NTN_M3_DBG_CNT_START + 4*5:  	MAC LPI/PWR/EVENT
	 * NTN_M3_DBG_CNT_START + 4*6:  	DMA RX0
	 *   .........................
	 * NTN_M3_DBG_CNT_START + 4*11:  	DMA RX5
	 * NTN_M3_DBG_CNT_START + 4*12: 	TDM
	 * NTN_M3_DBG_CNT_START + 4*13:  	CAN
	 * NTN_M3_DBG_CNT_START + 4*14:  	Reserved
	 * NTN_M3_DBG_CNT_START + 4*15:  	M3_MS_Ticks
	 */
