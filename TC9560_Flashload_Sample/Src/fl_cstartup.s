;
;********************************************************************************************************
;                                    EXCEPTION VECTORS & STARTUP CODE
;
; File      : cstartup.s
; For       : ARMv7M Cortex-M3
; Mode      : Thumb2
; Toolchain : RealView Development Suite
;             RealView Microcontroller Development Kit (MDK)
;             ARM Developer Suite (ADS)
;             Keil uVision
;********************************************************************************************************
;

;/*
;********************************************************************************************************
;*                           <<< Use Configuration Wizard in Context Menu >>>
;*
;* Note(s) : (1) The �Vision4 Configuration Wizard enables menu driven configuration of assembler, 
;*               C/C++, or debugger initialization files. The Configuration Wizard uses control items 
;*               that are embedded into the comments of the configuration file.
;*
;********************************************************************************************************
;*/

;/*
;********************************************************************************************************
;*                                              STACK DEFINITIONS
;*
;* Configuration Wizard Menu:
;* // <h> Stack Configuration
;* //   <o> Stack Size (in Bytes) <0x0-0xFFFFFFFF:8>
;* // </h>;
;*********************************************************************************************************
;*/

Stack_Size      EQU     0x00000400

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp    


;/*
;********************************************************************************************************
;*                                              STACK DEFINITIONS
; <h> Heap Configuration
;   <o>  Heap Size (in Bytes) <0x0-0xFFFFFFFF:8>
; </h>
;*********************************************************************************************************
;*/

Heap_Size       EQU     0x00000000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                THUMB


;/**************************************************************************/
;
; Vector Table Mapped to Address 0 at Reset
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors		
								IMPORT  SysTick_Handler


__Vectors       DCD     __initial_sp                 ;   0, Top of Stack
                DCD     Reset_Handler                ;   1, Reset Handler
                DCD     App_NMI_ISR                  ;   2, NMI Handler
                DCD     App_Fault_ISR                ;   3, Hard Fault Handler
                DCD     App_MemFault_ISR             ;   4, MPU Fault Handler
                DCD     App_BusFault_ISR             ;   5, Bus Fault Handler
                DCD     App_UsageFault_ISR           ;   6, Usage Fault Handler
                DCD     App_Spurious_ISR             ;   7, Reserved
                DCD     App_Spurious_ISR             ;   8, Reserved
                DCD     App_Spurious_ISR             ;   9, Reserved
                DCD     App_Spurious_ISR             ;  10, Reserved
                DCD     App_Spurious_ISR             ;  11, SVCall Handler
                DCD     App_Spurious_ISR             ;  12, Debug Monitor Handler
                DCD     App_Spurious_ISR             ;  13, Reserved
                DCD     App_Reserved_ISR             ;  14, PendSV Handler
                DCD     SysTick_Handler              ;  15, SysTick Handler

                ;  External Interrupts                                            
                DCD     App_Reserved_ISR             ;  16, INT0       [  0] Interrupt pin 0                                             
                DCD     App_Reserved_ISR             ;  17, INT1       [  1] Interrupt pin 1                                             
                DCD     App_Reserved_ISR             ;  18, INT2       [  2] Interrupt pin 2                                             
                DCD     App_Reserved_ISR             ;  19, INT3       [  3] Interrupt pin 3                                             
                DCD     App_Reserved_ISR             ;  20, INT4       [  4] Interrupt pin 4                                             
                DCD     App_Reserved_ISR             ;  21, INT5       [  5] Interrupt pin 5                                             
                DCD     App_Reserved_ISR             ;  22, INT6       [  6] Interrupt pin 6                                             
                DCD     App_Reserved_ISR             ;  23, INT7       [  7] Interrupt pin 7                                             
                DCD     App_Reserved_ISR             ;  24, INT8       [  8] Interrupt pin 8                                             
                DCD     App_Reserved_ISR             ;  25, INT9       [  9] Interrupt pin 9                                             
                DCD     App_Reserved_ISR             ;  26, INTA       [ 10] Interrupt pin A                                             
                DCD     App_Reserved_ISR             ;  27, INTB       [ 11] Interrupt pin B                                             
                DCD     App_Reserved_ISR             ;  28, INTC       [ 12] Interrupt pin C                                             
                DCD     App_Reserved_ISR             ;  29, INTD       [ 13] Interrupt pin D                                             
                DCD     App_Reserved_ISR             ;  30, INTE       [ 14] Interrupt pin E                                             
                DCD     App_Reserved_ISR             ;  31, INTF       [ 15] Interrupt pin F                                             
                DCD     App_Reserved_ISR             ;  32, INTRX0     [ 16] Serial reception (channel.0)                                
                DCD     App_Reserved_ISR             ;  33, INTTX0     [ 17] Serial transmission (channel.0)                             
                DCD     App_Reserved_ISR             ;  34, INTRX1     [ 18] Serial reception (channel.1)                                
                DCD     App_Reserved_ISR             ;  35, INTTX1     [ 19] Serial transmission (channel.1)                             
                DCD     App_Reserved_ISR             ;  36, INTRX2     [ 20] Serial reception (channel.2)                                
                DCD     App_Reserved_ISR             ;  37, INTTX2     [ 21] Serial transmission (channel.2)                             
                DCD     App_Reserved_ISR             ;  38, INTRX3     [ 22] Serial reception (channel.3)                                
                DCD     App_Reserved_ISR             ;  39, INTTX3     [ 23] Serial transmission (channel.3)                             
                DCD     App_Reserved_ISR             ;  40, INTUART4   [ 24] FULL UART(channel.4)                                        
                DCD     App_Reserved_ISR             ;  41, INTUART5   [ 25] FULL UART(channel.5)                                        
                DCD     App_Reserved_ISR             ;  42, INTSBI0    [ 26] Serial bus interface 0                                      
                DCD     App_Reserved_ISR             ;  43, INTSBI1    [ 27] Serial bus interface 1                                      
                DCD     App_Reserved_ISR             ;  44, INTSBI2    [ 28] Serial bus interface 2                                      
                DCD     App_Reserved_ISR             ;  45, INTSSP0    [ 29] SPI serial interface 0                                      
                DCD     App_Reserved_ISR             ;  46, INTSSP1    [ 30] SPI serial interface 1                                      
                DCD     App_Reserved_ISR             ;  47, INTSSP2    [ 31] SPI serial interface 2                                      
                DCD     App_Reserved_ISR             ;  48, INTUSBH    [ 32] USB Host Interrupt                                          
                DCD     App_Reserved_ISR             ;  49, INTUSBD    [ 33] USB Device Interrupt                                        
                DCD     App_Reserved_ISR             ;  50, INTUSBWKUP [ 34] USB WakeUp                                                  
                DCD     App_Reserved_ISR             ;  51, INTCANRX   [ 35] CAN RX                                                      
                DCD     App_Reserved_ISR             ;  52, INTCANTX   [ 36] CAN TX                                                      
                DCD     App_Reserved_ISR             ;  53, INTCANGB   [ 37] CAN STAUTS                                                  
                DCD     App_Reserved_ISR             ;  54, INTETH     [ 38] EtherNET Interrupt                                          
                DCD     App_Reserved_ISR             ;  55, INTETHWK   [ 39] EtherNET(magic packet detection) interrupt                  
                DCD     App_Reserved_ISR             ;  56, INTADAHP   [ 40] Highest priority AD conversion complete interrupt (channel.A)
                DCD     App_Reserved_ISR             ;  57, INTADAM0   [ 41] AD conversion monitoring function interrupt 0(channel.A)    
                DCD     App_Reserved_ISR             ;  58, INTADAM1   [ 42] AD conversion monitoring function interrupt 1(channel.A)    
                DCD     App_Reserved_ISR             ;  59, INTADA     [ 43] AD conversion interrupt(channel.A)                          
                DCD     App_Reserved_ISR             ;  60, INTADBHP   [ 44] Highest priority AD conversion complete interrupt (channel.B)
                DCD     App_Reserved_ISR             ;  61, INTADBM0   [ 45] AD conversion monitoring function interrupt 0(channel.B)    
                DCD     App_Reserved_ISR             ;  62, INTADBM1   [ 46] AD conversion monitoring function interrupt 1(channel.B)    
                DCD     App_Reserved_ISR             ;  63, INTADB     [ 47] AD conversion interrupt(channel.B)                          
                DCD     App_Reserved_ISR             ;  64, INTEMG0    [ 48] PMD0 EMG interrupt (MPT0)                                   
                DCD     App_Reserved_ISR             ;  65, INTPMD0    [ 49] PMD0 PWM interrupt (MPT0)                                   
                DCD     App_Reserved_ISR             ;  66, INTENC0    [ 50] PMD0 Encoder input interrupt (MPT0)                         
                DCD     App_Reserved_ISR             ;  67, INTEMG1    [ 51] PMD1 EMG interrupt (MPT1)                                   
                DCD     App_Reserved_ISR             ;  68, INTPMD1    [ 52] PMD1 PWM interrupt (MPT1)                                   
                DCD     App_Reserved_ISR             ;  69, INTENC1    [ 53] PMD1 Encoder input interrupt (MPT1)                         
                DCD     App_Reserved_ISR             ;  70, INTMTEMG0  [ 54] 16-bit MPT0 IGBT EMG interrupt                              
                DCD     App_Reserved_ISR             ;  71, INTMTPTB00 [ 55] 16-bit MPT0 IGBT period/ TMRB compare match detection 0     
                DCD     App_Reserved_ISR             ;  72, INTMTTTB01 [ 56] 16-bit MPT0 IGBT trigger/ TMRB compare match detection 1    
                DCD     App_Reserved_ISR             ;  73, INTMTCAP00 [ 57] 16-bit MPT0 input capture 0                                 
                DCD     App_Reserved_ISR             ;  74, INTMTCAP01 [ 58] 16-bit MPT0 input capture 1                                 
                DCD     App_Reserved_ISR             ;  75, INTMTEMG1  [ 59] 16-bit MPT1 IGBT EMG interrupt                              
                DCD     App_Reserved_ISR             ;  76, INTMTPTB10 [ 60] 16-bit MPT1 IGBT period/ TMRB compare match detection 0     
                DCD     App_Reserved_ISR             ;  77, INTMTTTB11 [ 61] 16-bit MPT1 IGBT trigger/ TMRB compare match detection 1    
                DCD     App_Reserved_ISR             ;  78, INTMTCAP10 [ 62] 16-bit MPT1 input capture 0                                 
                DCD     App_Reserved_ISR             ;  79, INTMTCAP11 [ 63] 16-bit MPT1 input capture 1                                 
                DCD     App_Reserved_ISR             ;  80, INTMTEMG2  [ 64] 16-bit MPT2 IGBT EMG interrupt     


                AREA    |.text|, CODE, READONLY


                                                      ; Reset Handler
Reset_Handler   PROC
                EXPORT  Reset_Handler             [WEAK]								
                IMPORT  __main
                LDR     R0, =__main
                BX      R0
                ENDP

                                                      ; Dummy Exception Handlers (infinite loops which can be modified)
App_NMI_ISR     PROC
                EXPORT  App_NMI_ISR               [WEAK]
                B       .
                ENDP

App_Fault_ISR   PROC
                EXPORT  App_Fault_ISR             [WEAK]
                B       .
                ENDP
App_MemFault_ISR\
                PROC
                EXPORT  App_MemFault_ISR          [WEAK]
                B       .
                ENDP
App_BusFault_ISR\
                PROC
                EXPORT  App_BusFault_ISR          [WEAK]
                B       .
                ENDP
App_UsageFault_ISR\
                PROC
                EXPORT  App_UsageFault_ISR        [WEAK]
                B       .
                ENDP
App_Spurious_ISR\
                PROC
                EXPORT  App_Spurious_ISR          [WEAK]
                B       .
                ENDP
App_Reserved_ISR\
                PROC
                EXPORT  App_Reserved_ISR          [WEAK]
                B       .
                ENDP
				

CPU_IntDis			PROC
				EXPORT CPU_IntDis
        CPSID   I
        BX      LR
				ENDP

CPU_IntEn			PROC
				EXPORT CPU_IntEn
        CPSIE   I
        BX      LR
				ENDP
		
				
				ALIGN


                ALIGN


                                                      ; User Initial Stack & Heap
                IF      :DEF:__MICROLIB
                
                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit
                
                ELSE
                
                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap
__user_initial_stackheap

                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR

                ALIGN

                ENDIF


                END
