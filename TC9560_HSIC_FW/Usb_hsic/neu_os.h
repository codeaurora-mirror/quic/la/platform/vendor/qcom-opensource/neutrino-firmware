/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 :  
 */
 
#ifndef NEU_OS_H
#define	NEU_OS_H


#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include "dwc_list.h"

#ifndef LM_INTERFACE
#define LM_INTERFACE
#endif

/* KERNEL ERROR CODES*/

#define ERESTARTSYS     512

/* SYSTEM ERROR CODES*/
#define ENOMEM          12      /* Out of memory */


#ifdef	__cplusplus
extern "C" {
#endif

typedef unsigned long long uint64_t;
typedef unsigned int uint32_t;
typedef unsigned short uint16_t;	
typedef unsigned char uint8_t;
typedef signed int int32_t;
	
typedef unsigned short uid16_t;

//typedef unsigned long dwc_dma_t;
typedef unsigned int dwc_dma_t;
	
#if 1
typedef int ssize_t;
#endif

extern uint32_t REG_RD(uint32_t volatile *reg);
extern void REG_WR(uint32_t volatile *reg, uint32_t value);

int neu_init(void) ;	
	
#define GFP_KERNEL      1	
/**
 * Modify bit values in a register.  Using the
 * algorithm: (reg_contents & ~clear_mask) | set_mask.
 */
extern void DWC_MODIFY_REG32(uint32_t volatile *reg, uint32_t clear_mask, uint32_t set_mask);

#define container_of(ptr, type, member) ({ \
    const typeof( ((type *)0)->member ) \
    *__mptr = (ptr); \
    (type *)( (char *)__mptr - offsetof(type,member) );}) 

		
 struct tasklet_struct
 {
         struct tasklet_struct *next;
         unsigned long state;
       // atomic_t count;
         void (*func)(unsigned long);
         unsigned long data;
};
struct lm_device; 
#if 0
struct device_driver {
  const char * name;
//  struct bus_type * bus;
 // struct module * owner;
  const char * mod_name;
//  bool suppress_bind_attrs;
 // const struct of_device_id * of_match_table;
//  const struct acpi_device_id * acpi_match_table;
  int (* probe) (struct device *dev);
  int (* remove) (struct device *dev);
  void (* shutdown) (struct device *dev);
//  int (* suspend) (struct device *dev, pm_message_t state);
  int (* resume) (struct device *dev);
 // const struct attribute_group ** groups;
 // const struct dev_pm_ops * pm;
 // struct driver_private * p;
}; 
#endif

struct device { 
				void* driver_data;
				struct device_driver * driver;

//				struct lm_device dev;
};
 
struct lm_device {
        struct device           dev;
//        struct resource         resource;
        unsigned int            irq;
        unsigned int            id;
};

 

/* Refered from Linux/include/linux/irqreturn.h */
/**
 * enum irqreturn
 * @IRQ_NONE            interrupt was not from this device
 * @IRQ_HANDLED         interrupt was handled by this device
 * @IRQ_WAKE_THREAD     handler requests to wake the handler thread
 */
  enum irqreturn {
          IRQ_NONE                = (0 << 0),
          IRQ_HANDLED             = (1 << 0),
          IRQ_WAKE_THREAD         = (1 << 1),
  };
  
  typedef enum irqreturn irqreturn_t;
  #define IRQ_RETVAL(x)   ((x) ? IRQ_HANDLED : IRQ_NONE)
 
struct list_head {
         struct list_head *next, *prev;
};
 
 
 #if 0
 static inline void *dev_get_drvdata(const struct device *dev)
 {
//         return dev->driver_data;
 }
#endif
#if 0
 
 /******* CH9.h *********/
 
 enum usb_device_state {
         /* NOTATTACHED isn't in the USB spec, and this state acts
          * the same as ATTACHED ... but it's clearer this way.
          */
         USB_STATE_NOTATTACHED = 0,
 
         /* chapter 9 and authentication (wireless) device states */
         USB_STATE_ATTACHED,
         USB_STATE_POWERED,                      /* wired */
         USB_STATE_RECONNECTING,                 /* auth */
         USB_STATE_UNAUTHENTICATED,              /* auth */
         USB_STATE_DEFAULT,                      /* limited function */
         USB_STATE_ADDRESS,
         USB_STATE_CONFIGURED,                   /* most functions */

         USB_STATE_SUSPENDED
 
        /* NOTE:  there are actually four different SUSPENDED
         * states, returning to POWERED, DEFAULT, ADDRESS, or
         * CONFIGURED respectively when SOF tokens flow again.
         * At this level there's no difference between L1 and L2
         * suspend states.  (L2 being original USB 1.1 suspend.)
         */
};
#endif



enum usb_device_speed {
         USB_SPEED_UNKNOWN = 0,                  /* enumerating */
         USB_SPEED_LOW, USB_SPEED_FULL,          /* usb 1.1 */
         USB_SPEED_HIGH,                         /* usb 2.0 */
         USB_SPEED_WIRELESS,                     /* wireless (usb 2.5) */
         USB_SPEED_SUPER                        /* usb 3.0 */
 };


#ifdef	__cplusplus
}
#endif

#endif	/* NEU_OS_H */

