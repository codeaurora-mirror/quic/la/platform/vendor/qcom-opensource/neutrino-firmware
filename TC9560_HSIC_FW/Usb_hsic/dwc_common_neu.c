/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      18 July 2016 : 
 */

#include "dwc_os.h"
#include "dwc_otg_dbg.h"

/* MISC */

void *DWC_MEMSET(void *dest, uint8_t byte, uint32_t size)
{
	return memset(dest, byte, size);
}

void *DWC_MEMCPY(void *dest, void const *src, uint32_t size)
{
	return memcpy(dest, src, size);
}

void *DWC_MEMMOVE(void *dest, void *src, uint32_t size)
{
	return memmove(dest, src, size);
}

int DWC_MEMCMP(void *m1, void *m2, uint32_t size)
{
	return memcmp(m1, m2, size);
}

int DWC_STRNCMP(void *s1, void *s2, uint32_t size)
{
	return strncmp(s1, s2, size);
}

int DWC_STRCMP(void *s1, void *s2)
{
	return strcmp(s1, s2);
}

int DWC_STRLEN(char const *str)
{
	return strlen(str);
}

char *DWC_STRCPY(char *to, char const *from)
{
	return strcpy(to, from);
}

int DWC_VSNPRINTF(char *str, int size, char *format, va_list args)
{
	return vsnprintf(str, size, format, args);
}


void *__DWC_ALLOC(void *mem_ctx, uint32_t size)
{
	void *buf = malloc((size_t)size);
	DBG_USB_Print(DBG_USB,"Malloc : A=%05x S=%d\r\n", buf, size);
	if (!buf) {
		DBG_Warn_Print("__DWC_ALLOC error\r\n");
		return NULL;
	}
	//memset(buf, 0, (size_t)size);
	return buf;
}

void __DWC_FREE(void *mem_ctx, void *addr)
{
	DBG_USB_Print(DBG_USB,"Free : A=%05x\r\n", addr);
	  free((void *)addr);
}




/* Register Acess */

void DWC_MODIFY_REG32(uint32_t volatile *reg, uint32_t clear_mask, uint32_t set_mask)
{
	*reg = ((*((uint32_t*)reg)) & ~clear_mask) | set_mask;
}




/* Timing */

void udelay(uint32_t usecs)
{
	while(usecs)
	{
			usecs--;
	}		
}

void DWC_UDELAY(uint32_t usecs)
{
	udelay(usecs);
}

void DWC_MDELAY(uint32_t msecs)
{
	udelay(msecs * 1000);
}

void DWC_MSLEEP(uint32_t msecs)
{
	//msleep(msecs);
	udelay(msecs * 1000);
}

//MODULE_DESCRIPTION("DWC Common Library - Portable version");
//MODULE_AUTHOR("Synopsys Inc.");
//MODULE_LICENSE ("GPL");
