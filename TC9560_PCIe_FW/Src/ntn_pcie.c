/* ============================================================================
 * The MIT License (MIT)
 *
 * Copyright (c) 2016 Toshiba Corp.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 * ========================================================================= */

/*! History:   
 *      25-Oct-2015 : Initial 
 */

/*
*********************************************************************************************************
*                                             INCLUDE FILES
*********************************************************************************************************
*/
#include "common.h"
#include "Neutrino.h"
#include "ntn_uart.h"


#define PINT0_INT_REG												0xA000		
#define PINT0_INT_MSK_REG										0xA004	
#define PINT1_INT_REG												0xA010	
#define PINT1_INT_MSK_REG										0xA014	
#define PINT2_INT_REG												0xA020	
#define PINT2_INT_MSK_REG										0xA024	
				
#define VC_VALID_INT_REG										0xA100	
#define VC_VALID_INT_MSK_REG								0xA104	
#define MAC_INFORM_INT_REG									0xA108	
#define MAC_INFORM_INT_MSK_REG							0xA10C	
#define RECEIVE_MSG_REG					  				  0xA110	
#define RECEIVE_MSG_MSK_REG			  				  0xA114	
#define RC_INTERNAL_INT_REG									0xA120	
#define RC_INTERNAL_INT_MSK_REG							0xA124	
#define POWER_INFORM_INT_REG								0xA128	
#define POWER_INFORM_INT_MSK_REG						0xA12C	
#define VC0_MAXI_ERR_REG										0xA280	
#define VC0_AXIW_ERR_REG										0xA300
#define VC0_AXIW_ERR_MSK_REG								0xA308
#define VC0_AXIR_ERR_REG										0xA310
#define VC0_AXIR_ERR_MSK_REG								0xA318
#define FLR_REG															0xA400
#define FLR_MSK_REG													0xA404	
#define FUNCTION_READY_REG									0xA40C
#define PMCSR_CHANGE_REG										0xA410
#define PMCSR_CHANGE_MSK_REG								0xA414
#define TR_PEND_BIT_COPY_REG								0xA500
#define TR_PEND_CHNAGE_DET_REG							0xA504
#define TR_PEND_CHNAGE_DET_MSK_REG					0xA508


/*
*********************************************************************************************************
*                                    LOCAL CONFIGURATION ERRORS
*********************************************************************************************************
*/
void NTN_GPIO_ISR_Handler(int num)
{
	unsigned int data;
	(void) num;
	data  = hw_reg_read32(0x40000000, 0x804C);
	
	if (data & 0x20) {
		data &= ~(0x20);
		hw_reg_write32(0x40000000, 0x804C, data);
		//NTN_POR_init_pcie();
	}
	else {
		data |= 0x20;
		hw_reg_write32(0x40000000, 0x804C, data);
		//NTN_POR_init_pcie();
	}
	
	//NTN_Ser_Printf("!!Host Reset!!\r\rn");
}

void NTN_PCIe_ISR_Handler(int num)
{
	unsigned int data, data2;
	
	switch (num) {
		case INT_SRC_NBR_PCIE_L12:
			break;
		
		case INT_SRC_NBR_PCIEC0:
			data = hw_reg_read32(NTN_PCIE_REG_BASE, PINT0_INT_REG);
			hw_reg_write32(NTN_PCIE_REG_BASE, PINT0_INT_REG, data);
			#ifdef PCIE_DEBUG
		  if(data & 0x00000001)   			NTN_Ser_Printf("PCLK valid\r\n");
			else if(data & 0x00000002)	{
				//NTN_POR_init_pcie();
				NTN_Ser_Printf("PCLK valid changed!\r\n");
			}
			else if(data & 0x00000100)		{
				NTN_Ser_Printf("Link State changed!\r\n");	
				if (data & 0xE00) {
					//NTN_POR_init_pcie();
				}
			}
		
			NTN_Ser_Printf("Link State: %d!\n", (data >>9) & 0x7);
			#endif
			break;
		case INT_SRC_NBR_PCIEC1:
			data = hw_reg_read32(NTN_PCIE_REG_BASE, PINT1_INT_REG);
			hw_reg_write32(NTN_PCIE_REG_BASE, PINT1_INT_REG, data);
			if(data & 0x00000001)   			{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, VC_VALID_INT_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, VC_VALID_INT_REG, data2);
				//NTN_Ser_Printf("VC valid changed\r\n");
			}
			else if(data & 0x00000002)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, MAC_INFORM_INT_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, MAC_INFORM_INT_REG, data2);
				//NTN_Ser_Printf("MAC INFORM INT!\r\n");
			}
			else if(data & 0x00000004)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, RECEIVE_MSG_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, RECEIVE_MSG_REG, data2);
				//NTN_Ser_Printf("Receive MSG INT\r\n");
			}
			else if(data & 0x00000008)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, RC_INTERNAL_INT_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, RC_INTERNAL_INT_REG, data2);
				//NTN_Ser_Printf("RC Internal INT!\r\n");
			}
			else if(data & 0x00000010)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, POWER_INFORM_INT_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, POWER_INFORM_INT_REG, data2);
				//NTN_Ser_Printf("PWR INFORM INT!\r\n");
			}
			else if(data & 0x00000100)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, 0xA200);
				hw_reg_write32(NTN_PCIE_REG_BASE, 0xA200, data2);
				//NTN_Ser_Printf("VC0 Integrity ERR!\r\n");
			}
			else if(data & 0x01000000)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, VC0_AXIW_ERR_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, VC0_AXIW_ERR_REG, data2);
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, VC0_AXIR_ERR_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, VC0_AXIR_ERR_REG, data2);
				//NTN_Ser_Printf("VC0 AXIR ERR!\r\n");
			}
			break;
		case INT_SRC_NBR_PCIEC2:
			data = hw_reg_read32(NTN_PCIE_REG_BASE, PINT2_INT_REG);
			hw_reg_write32(NTN_PCIE_REG_BASE, PINT2_INT_REG, data);
			if(data & 0x00000001)   			{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, FLR_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, FLR_REG, data2);
				//NTN_Ser_Printf("FLR INT\r\n");
			}
			else if(data & 0x00000008)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, PMCSR_CHANGE_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, PMCSR_CHANGE_REG, data2);
				//NTN_Ser_Printf("PMCSR changed!\r\n");
			}
			else if(data & 0x00000010)		{
				data2 = hw_reg_read32(NTN_PCIE_REG_BASE, TR_PEND_CHNAGE_DET_REG);
				hw_reg_write32(NTN_PCIE_REG_BASE, TR_PEND_CHNAGE_DET_REG, data2);
				//NTN_Ser_Printf("TR pend det!\r\n");
			}
			break;
			
		default:
			break;
	}
	return;
}

void NTN_PCIe_init(void)
{
	int i;
	
	#if 0 // GPIO Interrupts
	unsigned int data;
	
	// GPIO interrupts
	for (i = INT_SRC_NBR_GPIO9; i <= INT_SRC_NBR_GPIO11; i++) 
	{
		/* Enable the interrupt. */
		NVIC_EnableIRQ( (IRQn_Type)i );						
	}
	hw_reg_write32(0x40000000, 0x804C, 0x00); 
	hw_reg_write32(0x40000000, 0x100C, 0x20); 
	
	data = hw_reg_read32(0x40000000, 0x804C);
	if (data & 0x20) {
		data &= ~(0x20);
		hw_reg_write32(0x40000000, 0x804C, data);
	}
	else {
		data |= 0x20;
		hw_reg_write32(0x40000000, 0x804C, data);
	}
	hw_reg_write32(0x40000000, 0x804C, data);
	#endif
	
	#if 1
	// PCIe PORT Interrupts
	for (i = INT_SRC_NBR_PCIEC0; i <= INT_SRC_NBR_PCIE_L12; i++) 
	{
		/* Enable the interrupt. */
		NVIC_EnableIRQ( (IRQn_Type)i );	
	}
		
	hw_reg_write32(NTN_PCIE_REG_BASE, PINT0_INT_MSK_REG, 0);  // 0x01FF0102
	hw_reg_write32(NTN_PCIE_REG_BASE, PINT1_INT_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, PINT2_INT_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, VC_VALID_INT_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, MAC_INFORM_INT_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, RECEIVE_MSG_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, RC_INTERNAL_INT_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, POWER_INFORM_INT_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, VC0_AXIW_ERR_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, VC0_AXIR_ERR_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, FLR_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, PMCSR_CHANGE_MSK_REG, 0); 
	hw_reg_write32(NTN_PCIE_REG_BASE, TR_PEND_CHNAGE_DET_MSK_REG, 0); 	
	#endif
	
	return;
}

#if ( NTN_M3POR_4PCIE_ENABLE == DEF_ENABLED )
void NTN_POR_init_pcie()
{
	NTN_Ser_Printf("Start SW Seq\r\n");
	
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6018, 0x000000bc); 
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6020, 0x00000000);   
	NTN_Ser_Printf("Waiting for PCLK\r\n");
	while( !(hw_reg_read32(NTN_PCIE_REG_BASE, 0xA000) & 0x1) );
	NTN_Ser_Printf("PCLK availble!!!\r\n" );
	
	#if 0 // force PCIe to GEN1 only
	{
		unsigned int reg_val;
		reg_val = hw_reg_read32(NTN_PCIE_REG_BASE, 0x0070);
		reg_val &= ~0xF;
		reg_val |= 0x1;
		hw_reg_write32(NTN_PCIE_REG_BASE, 0x0070, reg_val); 
										
		//set bits 3:0 to 1 for reg 204c for gen1, keeping other bits
		reg_val = hw_reg_read32(NTN_PCIE_REG_BASE, 0x204C);
		reg_val &= ~0xF;
		reg_val |= 0x1;
		hw_reg_write32(NTN_PCIE_REG_BASE, 0x204C, reg_val); 
																	
		//set bits 7:1 to 1 for reg 206c for gen1
		reg_val = hw_reg_read32(NTN_PCIE_REG_BASE, 0x206C);
		reg_val = 0x2;
		hw_reg_write32(NTN_PCIE_REG_BASE, 0x206C, reg_val); 
		NTN_Ser_Printf("Force to GEN1!\r\n");
	}
	#endif
	
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6000, 0x00000001);  //added by sumant-san
	hw_reg_write32(NTN_PCIE_REG_BASE, 0xA000, 0x00000002);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x200C, 0x6C6D6B1C); 
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2008, 0x02000000);  // set to network controller
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2010, 0x40000004);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2014, 0x00000000);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2018, 0x0000000C); 
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x201C, 0x00000000);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2020, 0x1000000C);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2024, 0x00000000); 
	hw_reg_write32(NTN_PCIE_REG_BASE, 0xA108, 0x00000004);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0xA010, 0x00000002);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2FC0, 0x00000001); 
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2000, 0x021A1179);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x202C, 0x00011179);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x2090, 0x00000000);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x5000, 0x00000000);   	
	NTN_Ser_Printf("Finish SW POR Seq\r\n"); 
}


void config_tamap()
{
	/* TAMAP Configuration */
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6200, 0x00000000);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6204, 0x60000001);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x6208, 0x00000002);   
	hw_reg_write32(NTN_PCIE_REG_BASE, 0x620C, 0x20000023);   
}
#endif  //NTN_M3POR_4PCIE_ENABLE


