;/* ============================================================================
; * The MIT License (MIT)
; *
; * Copyright (c) 2016 Toshiba Corp.
; *
; * Permission is hereby granted, free of charge, to any person obtaining a copy
; * of this software and associated documentation files (the "Software"), to deal
; * in the Software without restriction, including without limitation the rights
; * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; * copies of the Software, and to permit persons to whom the Software is
; * furnished to do so, subject to the following conditions:
; *
; * The above copyright notice and this permission notice shall be included in
; * all copies or substantial portions of the Software.
; *
; * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
; * THE SOFTWARE.
; * ========================================================================= */
;
;/*! History:   
; *      25-Oct-2015 : Initial 
; */
;
;/*
;*********************************************************************************************************
;*                                              STACK DEFINITIONS
;*********************************************************************************************************
;*/

Stack_Size      EQU     0x00000400

                AREA    STACK, NOINIT, READWRITE, ALIGN=3
Stack_Mem       SPACE   Stack_Size
__initial_sp    


;/*
;*********************************************************************************************************
;*                                              STACK DEFINITIONS
;*********************************************************************************************************
;*/

Heap_Size       EQU     0x00000000

                AREA    HEAP, NOINIT, READWRITE, ALIGN=3
__heap_base
Heap_Mem        SPACE   Heap_Size
__heap_limit


                PRESERVE8
                THUMB


;/*
;*********************************************************************************************************
;* 													Vector Table
;*********************************************************************************************************
;*/
                AREA    RESET, DATA, READONLY
                EXPORT  __Vectors		
				IMPORT  SysTick_Handler

__Vectors       DCD     __initial_sp               ; 0:  Stack Pointer
                DCD     Reset_IRQHandler           ; 1:  Reset 
                DCD     NMI_IRQHandler             ; 2:  NMI 
                DCD     HardFault_IRQHandler       ; 3:  Hard Fault 
                DCD     MemManage_IRQHandler       ; 4:  MPU Fault 
                DCD     BusFault_IRQHandler        ; 5:  Bus Fault 
                DCD     UsageFault_IRQHandler      ; 6:  Usage Fault 
                DCD     App_Spurious_IRQHandler    ; 7:  App Spurious 
                DCD     App_Spurious_IRQHandler    ; 8:  App Spurious 
                DCD     App_Spurious_IRQHandler    ; 9:  App Spurious 
                DCD     App_Spurious_IRQHandler    ; 10: App Spurious 
                DCD     SVC_IRQHandler             ; 11: SVCall 
                DCD     DebugMon_IRQHandler        ; 12: Debug Monitor 
                DCD     App_Spurious_IRQHandler    ; 13: App Spurious 
                DCD     PendSV_IRQHandler          ; 14: PendSV 
                DCD     SysTick_Handler         ; 15: SysTick 
									         
                DCD     GPIO9_IRQHandler           ; 0:  Interrupt Pin 0.                         
                DCD     GPIO10_IRQHandler          ; 1:  Interrupt Pin 1.                         
                DCD     GPIO11_IRQHandler          ; 2:  Interrupt Pin 2.                         
                DCD     EXT_INT_IRQHandler         ; 3:  External Input Interrupt Pin: INTi       
                DCD     I2C_SLAVE_IRQHandler       ; 4:  I2C slave interrupt                      
                DCD     I2C_MASTER_IRQHandler      ; 5:  I2C mater interrupt                      
                DCD     SPI_SLAVE_IRQHandler       ; 6:  SPI slave interrupt                      
                DCD     HSIC_IRQHandler			   ; 7:  HSIC                                     
                DCD     MAC_LPI_EXIT_IRQHandler    ; 8:  eMAC LPI exit                                    
                DCD     MAC_POWER_IRQHandler       ; 9:  eMAC power managemenmt                           
                DCD     MAC_EVENTS_IRQHandler      ; 10: eMAC event from LPI, GRMII, Management counter...
                DCD     EMACTXDMA0_IRQHandler      ; 11: eMAC Tx DMA Channel 3. VLAN Q1 traffic  
                DCD     EMACTXDMA1_IRQHandler      ; 12: eMAC Tx DMA Channel 4. VLAN Q2 traffic  
                DCD     EMACTXDMA2_IRQHandler      ; 13: eMAC Tx DMA Channel 3. VLAN Q1 traffic  
                DCD     EMACTXDMA3_IRQHandler      ; 14: eMAC Tx DMA Channel 4. VLAN Q2 traffic  
                DCD     EMACTXDMA4_IRQHandler      ; 15: eMAC Tx DMA Channel 4. VLAN Q2 traffic  
									
                DCD     EMACRXDMA0_IRQHandler      ; 16: eMAC Rx DMA Channel 0. from Q0 all the other       
                DCD     EMACRXDMA1_IRQHandler      ; 17: eMAC Rx DMA Channel 1. from Q0 match M3 DA?        
                DCD     EMACRXDMA2_IRQHandler      ; 18: eMAC Rx DMA Channel 2. from Q0 match host DA?      
                DCD     EMACRXDMA3_IRQHandler      ; 19: eMAC Rx DMA Channel 3. from Q1 layer 2 gPTP        
                DCD     EMACRXDMA4_IRQHandler      ; 20: eMAC Rx DMA Channel 4. from Q2 untagged AV Control 
                DCD     EMACRXDMA5_IRQHandler      ; 21: eMAC Rx DMA Channel 5. from Q3 VLAN tagged         
                DCD     TDM0_IN_OV_IRQHandler      ; 22: TMD Channel 0 input overflow.        
                DCD     TDM1_IN_OV_IRQHandler      ; 23: TMD Channel 1 input overflow.        
                DCD     TDM2_IN_OV_IRQHandler      ; 24: TMD Channel 2 input overflow.        
                DCD     TDM3_IN_OV_IRQHandler      ; 25: TMD Channel 3 input overflow.        
                DCD     TDM_OUT_OV_IRQHandler      ; 26: TMD Channel output overflow.         
                DCD     TDM_OUT_UD_IRQHandler      ; 27: TMD Channel output underflow.        
                DCD     TDM_OUT_ERR_IRQHandler     ; 28: TMD Channel output general error.    

                DCD     QSPI_IRQHandler            ; 29: qSPI interrupt.   
                DCD     GDMA0_IRQHandler           ; 30: GDMA Channel 0.   
                DCD     GDMA1_IRQHandler           ; 31: GDMA Channel 1.   
                DCD     GDMA2_IRQHandler           ; 32: GDMA Channel 2.   
                DCD     GDMA3_IRQHandler           ; 33: GDMA Channel 3.   
                DCD     GDMA4_IRQHandler           ; 34: GDMA Channel 4.   
                DCD     GDMA5_IRQHandler           ; 35: GDMA Channel 5.   
                DCD     GDMAGEN0_IRQHandler        ; 36: GDMA General      

                DCD     SHA_IRQHandler             ; 37: SHA
                DCD     UART_IRQHandler            ; 38: UART
									
				DCD     CAN0Ln0_IRQHandler         ; 39: (0x27) CAN1
				DCD     CAN0Ln1_IRQHandler         ; 40: (0x28) CAN2
				DCD     PCIEC0_IRQHandler          ; 41: PCIe
				DCD     PCIEC1_IRQHandler          ; 42: PCIe
				DCD     PCIEC2_IRQHandler          ; 43: PCIe
				DCD     PCIE_L12_IRQHandler        ; 44: PCIe
				DCD     MCU_FLAG_IRQHandler        ; 45: MCU Flag
				DCD     CAN1Ln0_IRQHandler         ; 46: (0x2E) CAN1
				DCD     CAN1Ln1_IRQHandler         ; 47: (0x2F) CAN2	
				DCD     WDT_IRQHandler             ; 48: WatchDog Timer
                                
                AREA    |.text|, CODE, READONLY


Reset_IRQHandler   	PROC
                EXPORT  Reset_IRQHandler  								
                IMPORT  __main
                LDR     R0, =__main
                BX      R0
                ENDP
					
NMI_IRQHandler     	PROC
                EXPORT  NMI_IRQHandler  
                B       .
                ENDP
					
HardFault_IRQHandler	PROC
                EXPORT  HardFault_IRQHandler   
                B       .
                ENDP
					
MemManage_IRQHandler	PROC
                EXPORT  MemManage_IRQHandler      
                B       .
                ENDP
					
BusFault_IRQHandler	PROC
                EXPORT  BusFault_IRQHandler    
                B       .
                ENDP
					
UsageFault_IRQHandler	PROC
                EXPORT  UsageFault_IRQHandler    
                B       .
                ENDP
					
SVC_IRQHandler     	PROC
                EXPORT  SVC_IRQHandler   
                B       .
                ENDP
					
DebugMon_IRQHandler	PROC
                EXPORT  DebugMon_IRQHandler    
                B       .
                ENDP
					
PendSV_IRQHandler  	PROC
                EXPORT  PendSV_IRQHandler   
                B       .
                ENDP

App_Spurious_IRQHandler	PROC
                EXPORT  App_Spurious_IRQHandler   
                B       .
                ENDP
					
App_Reserved_IRQHandler	PROC
                EXPORT  App_Reserved_IRQHandler  
                B       .
                ENDP
																	
CPU_IntDis		PROC
				EXPORT CPU_IntDis
				CPSID   I
				BX      LR
				ENDP

CPU_IntEn		PROC
				EXPORT CPU_IntEn
				CPSIE   I
				BX      LR
				ENDP
		
Default_Handler PROC
                EXPORT  GPIO9_IRQHandler           [WEAK]
                EXPORT  GPIO10_IRQHandler          [WEAK]
                EXPORT  GPIO11_IRQHandler          [WEAK]
                EXPORT  EXT_INT_IRQHandler         [WEAK]
                EXPORT  I2C_SLAVE_IRQHandler       [WEAK]
                EXPORT  I2C_MASTER_IRQHandler      [WEAK]
                EXPORT  SPI_SLAVE_IRQHandler       [WEAK]
                EXPORT  HSIC_IRQHandler            [WEAK]
                EXPORT  MAC_LPI_EXIT_IRQHandler    [WEAK]
                EXPORT  MAC_POWER_IRQHandler       [WEAK]
                EXPORT  MAC_EVENTS_IRQHandler      [WEAK]
                EXPORT  EMACTXDMA0_IRQHandler      [WEAK]
                EXPORT  EMACTXDMA1_IRQHandler      [WEAK]
                EXPORT  EMACTXDMA2_IRQHandler      [WEAK]
                EXPORT  EMACTXDMA3_IRQHandler      [WEAK]
                EXPORT  EMACTXDMA4_IRQHandler      [WEAK]
                EXPORT  EMACRXDMA0_IRQHandler      [WEAK]
                EXPORT  EMACRXDMA1_IRQHandler      [WEAK]                
                EXPORT  EMACRXDMA2_IRQHandler      [WEAK]
                EXPORT  EMACRXDMA3_IRQHandler      [WEAK]
                EXPORT  EMACRXDMA4_IRQHandler      [WEAK]                              
                EXPORT  EMACRXDMA5_IRQHandler      [WEAK]
									
                EXPORT  TDM0_IN_OV_IRQHandler      [WEAK]
                EXPORT  TDM1_IN_OV_IRQHandler      [WEAK]
                EXPORT  TDM2_IN_OV_IRQHandler      [WEAK]
                EXPORT  TDM3_IN_OV_IRQHandler      [WEAK]
                EXPORT  TDM_OUT_OV_IRQHandler      [WEAK]
                EXPORT  TDM_OUT_UD_IRQHandler      [WEAK]
                EXPORT  TDM_OUT_ERR_IRQHandler     [WEAK]
                EXPORT  QSPI_IRQHandler            [WEAK]
                EXPORT  GDMA0_IRQHandler           [WEAK]
                EXPORT  GDMA1_IRQHandler           [WEAK]
                EXPORT  GDMA2_IRQHandler           [WEAK]
                EXPORT  GDMA3_IRQHandler           [WEAK]
                EXPORT  GDMA4_IRQHandler           [WEAK]
                EXPORT  GDMA5_IRQHandler           [WEAK]
                EXPORT  GDMAGEN0_IRQHandler        [WEAK]

                EXPORT  SHA_IRQHandler             [WEAK]
                EXPORT  UART_IRQHandler            [WEAK]
                EXPORT  CAN0Ln0_IRQHandler         [WEAK]
                EXPORT  CAN0Ln1_IRQHandler         [WEAK]
                EXPORT  PCIEC0_IRQHandler          [WEAK]
                EXPORT  PCIEC1_IRQHandler          [WEAK]
                EXPORT  PCIEC2_IRQHandler          [WEAK]
                EXPORT  PCIE_L12_IRQHandler        [WEAK]
                EXPORT  MCU_FLAG_IRQHandler        [WEAK]
                EXPORT  CAN1Ln0_IRQHandler         [WEAK]
                EXPORT  CAN1Ln1_IRQHandler         [WEAK]
                EXPORT  WDT_IRQHandler             [WEAK]

GPIO9_IRQHandler        
GPIO10_IRQHandler        
GPIO11_IRQHandler   
EXT_INT_IRQHandler    
I2C_SLAVE_IRQHandler  
I2C_MASTER_IRQHandler 
SPI_SLAVE_IRQHandler  
HSIC_IRQHandler       
MAC_LPI_EXIT_IRQHandler        
MAC_POWER_IRQHandler        
MAC_EVENTS_IRQHandler        
EMACTXDMA0_IRQHandler        
EMACTXDMA1_IRQHandler        
EMACTXDMA2_IRQHandler      
EMACTXDMA3_IRQHandler      
EMACTXDMA4_IRQHandler      
EMACRXDMA0_IRQHandler      
EMACRXDMA1_IRQHandler  
EMACRXDMA2_IRQHandler     
EMACRXDMA3_IRQHandler     
EMACRXDMA4_IRQHandler
EMACRXDMA5_IRQHandler
                        
TDM0_IN_OV_IRQHandler       
TDM1_IN_OV_IRQHandler     
TDM2_IN_OV_IRQHandler     
TDM3_IN_OV_IRQHandler      
TDM_OUT_OV_IRQHandler      
TDM_OUT_UD_IRQHandler      
TDM_OUT_ERR_IRQHandler      
QSPI_IRQHandler      
GDMA0_IRQHandler      
GDMA1_IRQHandler      
GDMA2_IRQHandler      
GDMA3_IRQHandler      
GDMA4_IRQHandler      
GDMA5_IRQHandler      
GDMAGEN0_IRQHandler   

SHA_IRQHandler       
UART_IRQHandler       
CAN0Ln0_IRQHandler     
CAN0Ln1_IRQHandler     
PCIEC0_IRQHandler   
PCIEC1_IRQHandler   
PCIEC2_IRQHandler   
PCIE_L12_IRQHandler 
MCU_FLAG_IRQHandler  
CAN1Ln0_IRQHandler
CAN1Ln1_IRQHandler
WDT_IRQHandler
                B       .
                ENDP


                ALIGN

; User Initial Stack & Heap
                IF      :DEF:__MICROLIB
                
                EXPORT  __initial_sp
                EXPORT  __heap_base
                EXPORT  __heap_limit
                
                ELSE
                
                IMPORT  __use_two_region_memory
                EXPORT  __user_initial_stackheap
__user_initial_stackheap

                LDR     R0, =  Heap_Mem
                LDR     R1, =(Stack_Mem + Stack_Size)
                LDR     R2, = (Heap_Mem +  Heap_Size)
                LDR     R3, = Stack_Mem
                BX      LR

                ALIGN

                ENDIF


                END
